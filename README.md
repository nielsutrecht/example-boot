# README #

This is a simple Spring Boot example project that shows how you can serve both dynamic and static content using Spring boot.

## Importing the project ##
* Fork this repo and then clone it to your own machine
* Import as Maven project (File > Import... > Maven > Exisiting Maven Projects in eclipse)
* Run com.example.ExampleController as Java Application
* Point your browser to localhost:8080

If everything is correct you should see a "Hello world" page.
