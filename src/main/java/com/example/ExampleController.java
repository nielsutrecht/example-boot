package com.example;

import java.text.SimpleDateFormat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@EnableAutoConfiguration
public class ExampleController {
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");

    @RequestMapping("sometxt")
    public String someFunction() throws JsonProcessingException {
        return "Some server side text";
    }

    @RequestMapping("time")
    public String time() throws JsonProcessingException {
        return Long.toString(System.currentTimeMillis());
    }

    public static void main(final String[] args) throws Exception {
        SpringApplication.run(ExampleController.class, args);
    }
}
