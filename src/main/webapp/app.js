$(function() {
    $.ajax({ url: "sometxt" })
        .done(function(txt) {
            $('#dynamicText').html(txt);
        });

    getTime();

    function getTime() {
       setTimeout(getTime, 1000);
       $.ajax({ url: "time" })
       .done(function(time) {
           var serverDate = new Date(parseInt(time));
           $('#serverTime').html(serverDate.toLocaleDateString() + ' ' + serverDate.toLocaleTimeString());
       });

       var clientDate = new Date();
       $('#clientTime').html(clientDate.toLocaleDateString() + ' ' + clientDate.toLocaleTimeString());
    }
});